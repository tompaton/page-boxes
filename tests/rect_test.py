from fractions import Fraction

from page_boxes.margin import Margin
from page_boxes.rect import Rect


def test_rect_str() -> None:
    assert str(Rect("1", "2")) == "Rect(width=Fraction(1, 1), height=Fraction(2, 1))"
    assert Rect("1", "2").str() == "Rect(1.000000, 2.000000)"


def test_subtract() -> None:
    assert Rect("10", "20").subtract(Margin("1", "2", "3", "4")) == Rect(width="7", height="13")


def test_center() -> None:
    assert Rect("10", "20").center(Rect("4", "8")) == Margin("3", "3", "6", "6")


def test_area() -> None:
    assert Rect("2", "3").area == Fraction(6)
