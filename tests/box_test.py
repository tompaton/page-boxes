from fractions import Fraction

import pytest

from page_boxes.box import Box, ImageBox, PdfBox
from page_boxes.margin import Margin
from page_boxes.rect import Rect


def test_box_pdfbox() -> None:
    # ignore types to avoid using Fraction(1)
    assert Box("1", "2", "3", "4").pdfbox(Fraction(10)).astuple() == (1, 4, 3, 4)


def test_box_bottom() -> None:
    assert Box("1", "2", "3", "4").bottom(Fraction(10)) == Fraction(4)


def test_box_area() -> None:
    assert Box("1", "2", "3", "4").area == Fraction(12)


@pytest.mark.parametrize(
    ("width", "height", "orientation"),
    [(10, 10, "square"), (10, 20, "portrait"), (20, 10, "landscape")],
)
def test_box_orientation(width: int, height: int, orientation: str) -> None:
    assert Box(1, 2, width, height).orientation == orientation  # type: ignore


def test_pdfbox_margin() -> None:
    assert str(PdfBox("1", "2", "3", "4").margin()) == "Margin(1.000000, 0.000000, 0.000000, 2.000000)"


def test_pdfbox_top() -> None:
    assert PdfBox("1", "4", "3", "4").top(Fraction(10)) == Fraction(2)


def test_pdfbox_aspect() -> None:
    assert PdfBox("1", "4", "3", "4").aspect == Fraction("0.75")


def test_pdfbox_offset() -> None:
    assert PdfBox("1", "2", "3", "4").offset(Margin("1", "2", "3", "4")).astuple() == (
        Fraction(2),
        Fraction(6),
        Fraction(3),
        Fraction(4),
    )


def test_pdfbox_subtract_margin() -> None:
    assert PdfBox("2", "6", "3", "4").subtract(Margin("1", "2", "3", "4")).astuple() == (
        Fraction(1),
        Fraction(2),
        Fraction(3),
        Fraction(4),
    )


@pytest.mark.parametrize(
    ("rect", "pagesize", "offset", "result"),
    [
        (PdfBox("1", "2", "3", "4"), Rect("10", "10"), 0, PdfBox("1", "2", "3", "4")),
        (PdfBox("5", "6", "10", "10"), Rect("10", "10"), 0, PdfBox("5", "6", "5", "4")),
        (PdfBox("1", "2", "10", "10"), Rect("10", "10"), 5, PdfBox("5", "2", "6", "8")),
        (
            PdfBox("5", "-2", "10", "10"),
            Rect("10", "10"),
            0,
            PdfBox("5", "0", "5", "8"),
        ),
    ],
)
def test_pdfbox_clip_rect(rect: PdfBox, pagesize: Rect, offset: int, result: PdfBox) -> None:
    assert rect.clip_rect(pagesize, Fraction(offset)) == result


@pytest.mark.parametrize(
    ("rect", "pagesize", "extend", "result"),
    [
        (PdfBox("1", "2", "3", "4"), Rect("10", "10"), "", PdfBox("1", "2", "3", "4")),
        (
            PdfBox("1", "2", "3", "4"),
            Rect("10", "10"),
            "left",
            PdfBox("0", "2", "4", "4"),
        ),
        (
            PdfBox("1", "2", "3", "4"),
            Rect("10", "10"),
            "right",
            PdfBox("1", "2", "9", "4"),
        ),
        (
            PdfBox("1", "2", "3", "4"),
            Rect("10", "10"),
            "bottom",
            PdfBox("1", "0", "3", "6"),
        ),
        (
            PdfBox("1", "2", "3", "4"),
            Rect("10", "10"),
            "top",
            PdfBox("1", "2", "3", "8"),
        ),
        (
            PdfBox("1", "2", "3", "4"),
            Rect("10", "10"),
            "left right",
            PdfBox("0", "2", "10", "4"),
        ),
        (
            PdfBox("1", "2", "3", "4"),
            Rect("10", "10"),
            "top bottom",
            PdfBox("1", "0", "3", "10"),
        ),
    ],
)
def test_pdfbox_extend_rect(rect: PdfBox, pagesize: Rect, extend: str, result: PdfBox) -> None:
    assert rect.extend_rect(extend, pagesize) == result


def test_pdfbox_pixels() -> None:
    assert PdfBox("1", "2", "3", "4").pixels(lambda v: int(v * 10)) == ImageBox(left=10, bottom=20, width=30, height=40)


def test_pdfbox_bounds() -> None:
    assert PdfBox("1", "2", "3", "4").bounds == (
        Fraction(1),
        Fraction(6),
        Fraction(4),
        Fraction(2),
    )
    assert PdfBox("2", "3", "4", "5").bounds == (
        Fraction(2),
        Fraction(8),
        Fraction(6),
        Fraction(3),
    )
    assert PdfBox("1", "2", "5", "6").bounds == (
        Fraction(1),
        Fraction(8),
        Fraction(6),
        Fraction(2),
    )


#
#  (1, 6)---(4, 6)
#  |             |
#  |             |
#  |    3 x 4    |
#  |             |
#  (1, 2)---(4, 2)
#
#
#  (2, 8)---(6, 8)
#  |             |
#  |             |
#  |    4 x 5    |
#  |             |
#  (2, 3)---(6, 3)
#


def test_pdfbox_add() -> None:
    assert PdfBox("1", "2", "3", "4").add(PdfBox("2", "3", "4", "5")) == PdfBox("1", "2", "5", "6")


@pytest.mark.parametrize(
    ("rects", "result"),
    [
        # empty
        ([], (None, None)),
        # right
        ([PdfBox("5", "2", "2", "4")], (0, None)),
        # closest right
        ([PdfBox("8", "2", "2", "4"), PdfBox("5", "2", "2", "4")], (1, None)),
        # down
        ([PdfBox("1", "-2", "3", "2")], (None, 0)),
        # closest down
        ([PdfBox("1", "-6", "3", "2"), PdfBox("1", "-2", "3", "2")], (None, 1)),
        # combination
        (
            [
                PdfBox("8", "2", "2", "4"),
                PdfBox("5", "2", "2", "4"),
                PdfBox("1", "-6", "3", "2"),
                PdfBox("1", "-2", "3", "2"),
            ],
            (1, 3),
        ),
        # none (left, up, diagonal)
        ([PdfBox("5", "3", "2", "4")], (None, None)),
        ([PdfBox("-5", "2", "2", "4")], (None, None)),
        ([PdfBox("2", "6", "3", "2")], (None, None)),
        ([PdfBox("1", "6", "3", "2")], (None, None)),
    ],
)
def test_pdfbox_get_adjacent(rects: list[PdfBox], result: tuple[int | None, int | None]) -> None:
    assert PdfBox("1", "2", "3", "4").get_adjacent(rects) == result


@pytest.mark.parametrize(
    ("rects", "span_direction", "result", "remaining"),
    [
        # empty
        ([], "right", PdfBox("1", "2", "3", "4"), []),
        ([], "down", PdfBox("1", "2", "3", "4"), []),
        # right
        ([PdfBox("5", "2", "2", "4")], "right", PdfBox("1", "2", "6", "4"), []),
        (
            [PdfBox("5", "2", "2", "4")],
            "down",
            PdfBox("1", "2", "3", "4"),
            [PdfBox("5", "2", "2", "4")],
        ),
        # down
        ([PdfBox("1", "-2", "3", "2")], "down", PdfBox("1", "-2", "3", "8"), []),
        (
            [PdfBox("1", "-2", "3", "2")],
            "right",
            PdfBox("1", "2", "3", "4"),
            [PdfBox("1", "-2", "3", "2")],
        ),
    ],
)
def test_pdfbox_merge_adjacent(
    rects: list[PdfBox], span_direction: str, result: PdfBox, remaining: list[PdfBox]
) -> None:
    assert PdfBox("1", "2", "3", "4").merge_adjacent(rects, span_direction) == result
    assert rects == remaining


@pytest.mark.parametrize(
    ("rects", "span_count", "span_direction", "result", "remaining"),
    [
        # empty
        ([], 1, "right", PdfBox("1", "2", "3", "4"), []),
        ([], 1, "down", PdfBox("1", "2", "3", "4"), []),
        # right
        ([PdfBox("5", "2", "2", "4")], 1, "right", PdfBox("1", "2", "6", "4"), []),
        (
            [PdfBox("5", "2", "2", "4")],
            1,
            "down",
            PdfBox("1", "2", "3", "4"),
            [PdfBox("5", "2", "2", "4")],
        ),
        ([PdfBox("5", "2", "2", "4")], 2, "right", PdfBox("1", "2", "6", "4"), []),
        (
            [PdfBox("5", "2", "2", "4")],
            0,
            "right",
            PdfBox("1", "2", "3", "4"),
            [PdfBox("5", "2", "2", "4")],
        ),
        # down
        ([PdfBox("1", "-2", "3", "2")], 1, "down", PdfBox("1", "-2", "3", "8"), []),
        (
            [PdfBox("1", "-2", "3", "2")],
            1,
            "right",
            PdfBox("1", "2", "3", "4"),
            [PdfBox("1", "-2", "3", "2")],
        ),
        ([PdfBox("1", "-2", "3", "2")], 2, "down", PdfBox("1", "-2", "3", "8"), []),
        (
            [PdfBox("1", "-2", "3", "2")],
            0,
            "down",
            PdfBox("1", "2", "3", "4"),
            [PdfBox("1", "-2", "3", "2")],
        ),
        # many right
        (
            [PdfBox("5", "2", "2", "4"), PdfBox("8", "2", "2", "4")],
            1,
            "right",
            PdfBox("1", "2", "6", "4"),
            [PdfBox("8", "2", "2", "4")],
        ),
        (
            [PdfBox("5", "2", "2", "4"), PdfBox("8", "2", "2", "4")],
            2,
            "right",
            PdfBox("1", "2", "9", "4"),
            [],
        ),
        # many down
        (
            [PdfBox("1", "-2", "3", "2"), PdfBox("1", "-5", "3", "2")],
            1,
            "down",
            PdfBox("1", "-2", "3", "8"),
            [PdfBox("1", "-5", "3", "2")],
        ),
        (
            [PdfBox("1", "-2", "3", "2"), PdfBox("1", "-5", "3", "2")],
            2,
            "down",
            PdfBox("1", "-5", "3", "11"),
            [],
        ),
    ],
)
def test_pdfbox_span_adjacent_rects(
    rects: list[PdfBox],
    span_count: int,
    span_direction: str,
    result: PdfBox,
    remaining: list[PdfBox],
) -> None:
    assert PdfBox("1", "2", "3", "4").span_adjacent_rects(rects, span_count, span_direction) == result
    assert rects == remaining


def test_pdfbox_annotation() -> None:
    assert PdfBox("1", "2", "3", "4").annotate("abc").add(PdfBox("2", "3", "4", "5").annotate("def")).annotation == [
        "abc",
        "def",
    ]
