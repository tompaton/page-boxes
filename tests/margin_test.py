from fractions import Fraction

from page_boxes.margin import Margin, MarginDefinition


def test_margin_str() -> None:
    assert str(Margin()) == "Margin(0.000000, 0.000000, 0.000000, 0.000000)"


def test_margin_add() -> None:
    assert (
        str(Margin("1", "2", "3", "4").add(Margin("5", "6", "7", "8")))
        == "Margin(6.000000, 8.000000, 10.000000, 12.000000)"
    )


def test_margin_offset() -> None:
    assert str(Margin("1", "2", "3", "4").offset(Fraction(2))) == "Margin(3.000000, 0.000000, 3.000000, 4.000000)"


def test_even() -> None:
    assert str(MarginDefinition("2", "1").even()) == "Margin(1.000000, 2.000000, 1.000000, 1.000000)"


def test_odd() -> None:
    assert str(MarginDefinition("2", "1").odd()) == "Margin(2.000000, 1.000000, 1.000000, 1.000000)"


def test_cover() -> None:
    assert str(MarginDefinition("2", "1").cover()) == "Margin(1.000000, 1.000000, 1.000000, 1.000000)"
