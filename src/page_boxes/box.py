from collections.abc import Callable
from dataclasses import dataclass, field
from fractions import Fraction
from typing import Any

from page_boxes.margin import Margin
from page_boxes.rect import ImageRect, Rect


@dataclass
class Box(Rect):
    # cm (or fractions if page width is 1.0 or 100 or ...)
    left: Fraction
    top: Fraction

    def __init__(
        self,
        left: Fraction | str,
        top: Fraction | str,
        width: Fraction | str,
        height: Fraction | str,
    ) -> None:
        super().__init__(width=width, height=height)
        self.left = Fraction(left)
        self.top = Fraction(top)

    def pdfbox(self, page_height: Fraction) -> "PdfBox":
        return PdfBox(
            self.left,
            self.bottom(page_height),
            self.width,
            self.height,
        )

    def bottom(self, page_height: Fraction) -> Fraction:
        return page_height - self.top - self.height


@dataclass(kw_only=True)
class ImageBox(ImageRect):
    # pixels
    left: int
    bottom: int

    def top(self, page_height: int) -> int:
        return page_height - self.bottom - self.height


@dataclass
class PdfBox(Rect):
    # cm
    left: Fraction
    bottom: Fraction
    _data: list[Any] = field(default_factory=list)

    def __init__(
        self,
        left: Fraction | str,
        bottom: Fraction | str,
        width: Fraction | str,
        height: Fraction | str,
        _data: list[Any] | None = None,
    ) -> None:
        super().__init__(width=width, height=height)
        self.left = Fraction(left)
        self.bottom = Fraction(bottom)
        self._data = _data or []

    def astuple(self) -> tuple[Fraction, Fraction, Fraction, Fraction]:
        return self.left, self.bottom, self.width, self.height

    def annotate(self, *data: Any) -> "PdfBox":
        self._data.extend(data)
        return self

    @property
    def annotation(self) -> list[Any]:
        return self._data

    def margin(self) -> Margin:
        return Margin(left=self.left, bottom=self.bottom)

    def top(self, page_height: Fraction) -> Fraction:
        return page_height - self.bottom - self.height

    def offset(self, margin: Margin) -> "PdfBox":
        return type(self)(
            self.left + margin.left,
            self.bottom + margin.bottom,
            self.width,
            self.height,
        ).annotate(*self.annotation)

    def subtract(self, margin: Margin) -> "PdfBox":
        return type(self)(
            self.left - margin.left,
            self.bottom - margin.bottom,
            self.width,
            self.height,
        ).annotate(*self.annotation)

    def add(self, other: "PdfBox") -> "PdfBox":
        left = min(self.left, other.left)
        right = max(self.left + self.width, other.left + other.width)
        bottom = min(self.bottom, other.bottom)
        top = max(self.bottom + self.height, other.bottom + other.height)
        return (
            type(self)(left, bottom, right - left, top - bottom).annotate(*self.annotation).annotate(*other.annotation)
        )

    @property
    def bounds(self) -> tuple[Fraction, Fraction, Fraction, Fraction]:
        return (
            self.left,
            self.bottom + self.height,
            self.left + self.width,
            self.bottom,
        )

    def span_adjacent_rects(self, rects: list["PdfBox"], span_count: int, span_direction: str) -> "PdfBox":
        if span_count:
            # span additional slots if necessary
            rect2 = self.merge_adjacent(rects, span_direction)

            return rect2.span_adjacent_rects(rects, span_count - 1, span_direction)

        return self

    def merge_adjacent(self, rects: list["PdfBox"], span_direction: str) -> "PdfBox":
        right_adjacent, down_adjacent = self.get_adjacent(rects)
        if span_direction == "right" and right_adjacent is not None:
            return self.add(rects.pop(right_adjacent))
        if span_direction == "down" and down_adjacent is not None:
            return self.add(rects.pop(down_adjacent))
        return self

    def get_adjacent(self, rects: list["PdfBox"]) -> tuple[int | None, int | None]:
        right_adjacent = None
        down_adjacent = None
        min_distance_right = Fraction("1000000000")
        min_distance_down = Fraction("1000000000")
        x1, y1, x2, y2 = self.bounds
        for i, rect in enumerate(rects):
            rx1, ry1, rx2, ry2 = rect.bounds
            distance_right = rx1 - x2
            if y1 == ry1 and y2 == ry2 and 0 <= distance_right < min_distance_right:
                right_adjacent = i
                min_distance_right = distance_right
            distance_down = y2 - ry1
            if x1 == rx1 and x2 == rx2 and 0 <= distance_down < min_distance_down:
                down_adjacent = i
                min_distance_down = distance_down

        return right_adjacent, down_adjacent

    def clip_rect(self, pagesize: Rect, offset: Fraction) -> "PdfBox":
        # clip any part of image_rect that goes outside the page boundary
        left, bottom, width, height = self.astuple()

        if left < offset:
            width -= offset - left
            left = offset

        if bottom < 0:
            height += bottom
            bottom = Fraction(0)

        if left + width > offset + pagesize.width:
            width = offset + pagesize.width - left

        if bottom + height > pagesize.height:
            height = pagesize.height - bottom

        return type(self)(left, bottom, width, height).annotate(*self.annotation)

    def extend_rect(self, extend: str, pagesize: Rect) -> "PdfBox":
        # extend image_rect to the page boundary
        left, bottom, width, height = self.astuple()

        if "left" in extend:
            width += left
            left = Fraction(0)

        if "right" in extend:
            width = pagesize.width - left

        if "bottom" in extend:
            height += bottom
            bottom = Fraction(0)

        if "top" in extend:
            height = pagesize.height - bottom

        return type(self)(left, bottom, width, height).annotate(*self.annotation)

    def pixels(self, cm_to_pixels: Callable[[float | Fraction], int]) -> ImageBox:
        return ImageBox(
            left=cm_to_pixels(self.left),
            bottom=cm_to_pixels(self.bottom),
            width=cm_to_pixels(self.width),
            height=cm_to_pixels(self.height),
        )
