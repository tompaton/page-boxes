from dataclasses import dataclass
from fractions import Fraction

from page_boxes.margin import Margin


@dataclass
class ImageRect:
    # pixels
    width: int
    height: int


@dataclass
class Rect:
    # cm
    width: Fraction
    height: Fraction

    def __init__(self, width: Fraction | str, height: Fraction | str) -> None:
        self.width = Fraction(width)
        self.height = Fraction(height)

    def subtract(self, margin: Margin) -> "Rect":
        return Rect(
            self.width - margin.left - margin.right,
            self.height - margin.top - margin.bottom,
        )

    def center(self, inner: "Rect") -> Margin:
        """Return a Margin that centers the inner Rect within this one."""
        offset_x = (self.width - inner.width) / 2
        offset_y = (self.height - inner.height) / 2
        return Margin(offset_x, offset_x, offset_y, offset_y)

    @property
    def area(self) -> Fraction:
        return self.width * self.height

    @property
    def aspect(self) -> Fraction:
        return self.width / self.height

    @property
    def orientation(self) -> str:
        if self.width == self.height:
            return "square"

        if self.width > self.height:
            return "landscape"

        return "portrait"

    def str1(self) -> str:
        return f"Rect(width={float_str(self.width)}, height={float_str(self.height)})"

    def str(self) -> str:
        return f"Rect({self.width:f}, {self.height:f})"


def float_str(f: Fraction) -> str:
    s = str(float(f)).rstrip("0")
    if s.endswith("."):
        return s + "0"
    return s
