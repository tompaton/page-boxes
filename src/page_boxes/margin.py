from dataclasses import dataclass
from fractions import Fraction


@dataclass
class Margin:
    left: Fraction
    right: Fraction
    top: Fraction
    bottom: Fraction

    def __init__(
        self,
        left: Fraction | str = Fraction(0),
        right: Fraction | str = Fraction(0),
        top: Fraction | str = Fraction(0),
        bottom: Fraction | str = Fraction(0),
    ) -> None:
        # cm
        self.left = Fraction(left)
        self.right = Fraction(right)
        self.top = Fraction(top)
        self.bottom = Fraction(bottom)

    def __str__(self) -> str:
        return f"Margin({self.left:f}, {self.right:f}, {self.top:f}, {self.bottom:f})"

    def add(self, margin: "Margin") -> "Margin":
        return Margin(
            self.left + margin.left,
            self.right + margin.right,
            self.top + margin.top,
            self.bottom + margin.bottom,
        )

    def offset(self, offset: Fraction) -> "Margin":
        return self.add(Margin(left=offset, right=-offset))


class MarginDefinition:
    def __init__(self, inside: Fraction | str, outside: Fraction | str) -> None:
        # cm
        self.inside = Fraction(inside)
        self.outside = Fraction(outside)

    def even(self) -> Margin:
        return Margin(self.outside, self.inside, self.outside, self.outside)

    def odd(self) -> Margin:
        return Margin(self.inside, self.outside, self.outside, self.outside)

    def cover(self) -> Margin:
        return Margin(self.outside, self.outside, self.outside, self.outside)
