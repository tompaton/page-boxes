# Page Boxes

Module computing margins and rectangles for layout of photobook pdf.

[![PyPI - Version](https://img.shields.io/pypi/v/page-boxes.svg)](https://pypi.org/project/page-boxes)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/page-boxes.svg)](https://pypi.org/project/page-boxes)

-----

## Table of Contents

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install page-boxes
```

## License

`page-boxes` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
